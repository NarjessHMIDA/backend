package com.narjess.rest.webservices.basic.auth;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//controller


@RestController
@CrossOrigin(origins= "http://localhost:3000")
public class BasicAuthController {
	
	
	@GetMapping(path = "/basicauth")
	public AuthentificationBean HelloWorldBean() {
		
		
		
		return new AuthentificationBean("Your are Authentificated");
	}
		
	
}
