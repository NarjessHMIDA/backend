package com.narjess.rest.webservices.todo;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@RestController 
@CrossOrigin(origins= "http://localhost:3000", maxAge = 3600)

public class TodoResource {

	@Autowired
	private TodoHardCodedService todoService; 
	
	@GetMapping("/users/{username}/todos")
	public List<Todo> getAlltodos(@PathVariable String username) { 
		//Thread.sleep(3000);
		return todoService.findAll();	
	}
	@GetMapping("/users/{username}/todos/{id}")
	public Todo getTodo(@PathVariable String username, @PathVariable long id ) { 
		return todoService.findById(id);	
	}
	
	//Delete /users/{username}/todos/{id}
	
	@DeleteMapping( "/users/{username}/todos/{id}")
	public ResponseEntity<Void> deleteTodo
	(@PathVariable String username, @PathVariable long id ){
			Todo todo =	todoService.deleteById(id);
			if(todo!=null) {
				return ResponseEntity.noContent().build();
		
	}
			return ResponseEntity.notFound().build();
	}
	
	//Edit /update a todo
	//Put /users/{user_name}/todos/{todo_id}
	
	@PutMapping(path = "/users/{username}/todos/{id}")
	public ResponseEntity<Todo> UpdateTodo(
			@PathVariable String username,
			@PathVariable long id, @RequestBody Todo todo ){
		
		Todo todoUpdated = todoService.save(todo);
		return new ResponseEntity<Todo>(todo, HttpStatus.OK);
	}
	//Create a new todo
	//Post /users/{user_name/todos/
	
@PostMapping("/users/{username}/todos")
	
	public ResponseEntity<Void> UpdateTodo
	(@PathVariable String username, @RequestBody Todo todo ){
		
		Todo CreatedTodo = todoService.save(todo);
		
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").
		buildAndExpand(CreatedTodo.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	
	}
	
	
	
	
	

