package com.narjess.rest.webservices.helloworld;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.narjess.rest.webservices.helloworld.HelloWorldBean;

//controller


@RestController
@CrossOrigin(origins= "http://localhost:3000")
public class HelloWorldController {
	
	//Get
	//URI -/hello-world
	//method -"hello World
	
	@GetMapping( path = "/hello-world")
	public String helloWorld() {
		return "hello world";
	}
	//hello-world-bean
	@GetMapping(path = "/hello-world-bean")
	public String HelloWorldBean() {
		return "hello world bean";
	}
		//hello-world/path-variable/narjess
	@GetMapping(path ="/hello-world/path-variable/{name}")
	public HelloWorldBean HelloWorldPathVariable(@PathVariable String name) {
		
		//throw new RuntimeException("something went wrong");
		return new  HelloWorldBean(String.format ("hello world, %s", name));
	}
	
	
}
